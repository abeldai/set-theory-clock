package com.paic.arch.interviews.impl;

import com.paic.arch.interviews.TimeConverter;
import com.paic.arch.interviews.logic.TimeConverterLogic;

import java.util.ArrayList;
import java.util.List;

/**
 * 时针模拟器接口实现类
 * @author: daizhong
 * @createTime: 18/2/4
 */
public class TimeConverterImpl implements TimeConverter {

    /**
     * 将时间转换成电子显示码
     * @param aTime
     * @return
     */
    @Override
    public String convertTime(String aTime){
        //获取时间参数
        String[] timeParam=aTime.split(":");
        Integer hour=Integer.parseInt(timeParam[0]);
        Integer minute=Integer.parseInt(timeParam[1]);
        Integer seconds=Integer.parseInt(timeParam[2]);
        List<String[]> resultList=new ArrayList<>(5);
        //获取每行转换后的编码
        TimeConverterLogic tcl=new TimeConverterLogic();
        tcl.getFirstRowCode(seconds,resultList);
        tcl.getSecondAndThirdRowCode(hour,resultList);
        tcl.getFourthAndFifthRowCode(minute,resultList);
        //获取结果
        String result=tcl.getArrayListChar(resultList);
        System.out.println("时间:"+aTime+",返回:\n"+result);
        return result;
    }


}
