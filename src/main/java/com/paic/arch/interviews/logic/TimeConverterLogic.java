package com.paic.arch.interviews.logic;

import java.util.List;

/**
 * 时针模拟器逻辑实现类
 * @author: daizhong
 * @createTime: 18/2/4
 */
public class TimeConverterLogic {


    /**
     * 获取第一行显示器编码
     * 规则:1个灯,偶数赋值"Y",奇数赋值"O"
     * @param seconds 秒
     * @param resultList 编码结果list
     */
    public void getFirstRowCode(int seconds,List<String[]> resultList){
        String[] firstRow=initRowsArray(1);
        if(0 == seconds % 2){
            firstRow[0]="Y";
        }
        resultList.add(firstRow);
    }

    /**
     * 获取第二和第三行显示器编码
     * 第二行规则:4个灯,每个灯代表5个小时,亮赋值"R",不亮赋值"O"
     * 第三行规则:4个灯,每个灯代表1个小时,亮赋值"R",不亮赋值"O"
     * @param hour 小时
     * @param resultList 编码结果list
     */
    public void getSecondAndThirdRowCode(int hour,List<String[]> resultList){
        //secondRow
        String[] secondRow=initRowsArray(4);
        int h1=hour/5;
        for(int i=0;i<h1;i++){
            secondRow[i]="R";
        }
        resultList.add(secondRow);
        //thirdRow
        String[] thirdRow=initRowsArray(4);
        int h2=hour-5*h1;
        for(int i=0;i<h2;i++){
            thirdRow[i]="R";
        }
        resultList.add(thirdRow);
    }

    /**
     * 获取第四和第五行显示器编码
     * 第四行规则:11个灯,每个灯5分钟,亮赋值"Y",3.6.9亮代表一个小时的一刻钟,赋值"R"
     * 第五行规则:4个灯,每个灯1分钟,亮赋值"Y"
     * @param minute
     * @param resultList
     */
    public void getFourthAndFifthRowCode(int minute,List<String[]> resultList){
        //fourthRow
        String[] fourthRow=initRowsArray(11);
        int m1=minute/5;
        int quarter=(minute/15)%4;
        for(int i=0;i<m1;i++){
            fourthRow[i]="Y";
            //判断刻钟
            for(int j=1;j<=quarter;j++){
                fourthRow[j*3-1]="R";
            }
        }
        resultList.add(fourthRow);
        //fifthRow
        String[] fifthRow=initRowsArray(4);
        int m2=minute-m1*5;
        for(int i=0;i<m2;i++){
            fifthRow[i]="Y";
        }
        resultList.add(fifthRow);
    }


    /**
     * 初始化每行数组,默认置为不亮("O")
     * @param arraySize
     * @return
     */
    public String[] initRowsArray(int arraySize){
        String[] newArray=new String[arraySize];
        for(int i=0;i<newArray.length;i++){
            newArray[i]="O";
        }
        return newArray;
    }


    /**
     * 获取arrayList结果数组的值
     * @param arrayList
     * @return
     */
    public String getArrayListChar(List<String[]> arrayList){
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<arrayList.size();i++){
            String[] row=arrayList.get(i);
            for(int j=0;j<row.length;j++){
                sb.append(row[j]);
            }
            if(i != (arrayList.size()-1)){
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}
